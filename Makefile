PORT=3333
APPPORT=80
APPNAME=covid-dashboard

build:
	docker build -t ${APPNAME}:latest .

push:
	docker tag ${APPNAME}:latest registry.gitlab.com/alxndr13/container-mirror/${APPNAME}:latest
	docker push registry.gitlab.com/alxndr13/container-mirror/${APPNAME}:latest

deploy:
	ssh hetzner sudo docker pull registry.gitlab.com/alxndr13/container-mirror/${APPNAME}:latest
	ssh hetzner sudo docker rm -f ${APPNAME} || true
	ssh hetzner sudo docker run -d --restart=always --name ${APPNAME} -e MONGODB_USERNAME=covid -e MONGODB_PASSWORD=${MONGODB_PASSWORD} -e MONGODB_URL=${MONGODB_URL} -p 127.0.0.1:${PORT}:${APPPORT} --restart always registry.gitlab.com/alxndr13/container-mirror/${APPNAME}:latest
